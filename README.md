# Secure database quering with `pg-aura`

This service automates database table management and provides methods for creating prepared statements for risky database queries. To use it you install the `pg` service and invoke it in you own:
```
    const pg = sm.get_service_instance("pg");
```

## Table creation

```
  const example_table = await pg.table(`example_table_name`, {
    id: 'uuid PRIMARY KEY DEFAULT gen_random_uuid()',
    type: "varchar(3)",
    name: "varchar(128)",
    description: "text",
    owner: "UUID REFERENCES example_accounts(id)"
  });
```

## Prepared statements

There are four methods that the table class hold: `insert`, `select`, `update` and `delete`. All of those are meant to simplify the creation of prepared statements.

### Insert

```
    await example_table.insert(
      ["type", "name", "description", "owner"],
      ["$1", "$2", "$3", "$4"],
      [
        "srv",
        "some-twig",
        "This twig does this and that",
        "uuid-1234-5678-..."
      ]
    )
```

### Select

```
    await example_table.select(['id', 'name'], "type = $1", ["lib"])
```

### Update

```
    await example_table.update([
      `description = $1`
    ], 'owner = $2', [
      "new description",
      "uuid-1234-5678-..."
    ]);
```

### Delete

```
    await example_table.delete('owner = $1', [ "uuid-1234-5678-..." ]);
```


