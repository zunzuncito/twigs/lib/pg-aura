

export default class ExpressionParser {
  static parse_val(cols, val_exprs, vals, ps_params, table) {
    for (let e = 0; e < val_exprs.length; e++) {
      const vexpr = val_exprs[e];

      let col_type = table.column(cols[e]).type;
      if (col_type.endsWith('[]')) col_type = col_type.slice(0, -2);

      const vindexes = vexpr.replace(/('.*?')/g, '').match(/(?<=\$)[0-9]+/g);
      for (let vi of vindexes) {
        
        ps_params[vi-1] = Array.isArray(vals[vi-1]) ?
          col_type+'[]' : col_type;
      }

    }
  }

  static parse_where(where_expr, vals, ps_params, table) {
    const wexpr_split = where_expr ? where_expr.split(/ *(?:AND|OR) */) : [];
    for (let wsplit of wexpr_split) {
      const wsmatches = wsplit.match(/(?:[A-Z\(]+)?([a-z_]+)(?:[a-z\>\-\)\'\s]+)?(?:[\s]+)?(ILIKE|LIKE|ALL|IN|<=|>=|!=|<>|@>|<@|&&|\|\||<|>|=)?(?:[\s]+)(.*$)/m);


      let col_type = table.column(wsmatches[1]).type;
      if (col_type == "jsonb" && wsmatches[2] == "LIKE") col_type = 'text'; 
      if (col_type.endsWith('[]')) col_type = col_type.slice(0, -2);

      const vindexes = wsmatches[3].match(/(?<=\$)[0-9]+/g);
      for (let vi of vindexes) {
        ps_params[vi-1] = Array.isArray(vals[vi-1]) ?
          col_type+'[]' : col_type;
      }
    }

  }

  static parse_set(set_exprs, vals, ps_params, table) {
    for (let e = 0; e < set_exprs.length; e++) {
      const sexpr = set_exprs[e];

      let col_type = table.column(sexpr.match(/(^[a-z_]*)(?=[ =]*.*$)/m)[0]).type;
      if (col_type.endsWith('[]')) col_type = col_type.slice(0, -2);

      const vindexes = sexpr.replace(/('.*?')/g, '').match(/(?<=\$)[0-9]+/g);
      for (let vi of vindexes) {
        ps_params[vi-1] = Array.isArray(vals[vi-1]) ?
          col_type+'[]' : col_type;
      }
    }
  }

  static parse_param_val(ps_vals, ps_params) {
    for (let v = 0; v < ps_vals.length; v++) {
      const value = ps_vals[v];

      if (typeof value === 'string') {
        ps_vals[v] = `'${value}'`;
      } else if (typeof value === 'array' || Array.isArray(value)) {
        ps_vals[v] = `ARRAY[${this.parse_param_val(value)}]::${ps_params[v]}`;
      } else if (value && typeof value === 'object') {
        ps_vals[v] = `'${JSON.stringify(value)}'::JSONB`;
      } else {
        ps_vals[v] = value;
      }
    }
    return ps_vals;
  }

  static parse_return(query_result, options) {
    if (!options) return;
    const return_match = options.match(/(?<=RETURNING|returning)(.*)$/m);
    if (return_match) {
      const return_cols = return_match[0].split(',');
      const return_obj = {};
      for (let r = 0; r < return_cols.length; r++) {
        const rcol_name = return_cols[r].split('AS')[0].trim().toLowerCase();
        return_obj[rcol_name] = query_result.rows[0][rcol_name];
      }

      return return_obj;
    }
  }
}


