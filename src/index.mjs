import pg from 'pg'

import Table from './table/table.mjs'

import QueryCrafter from './query/crafter.mjs'
import PraparedStatementManager from './ps/mg.mjs'

export default class PGAura {
  constructor(client, database) {
    this.client = client;
    this.database = database;

    this.psmg = new PraparedStatementManager(this);

    this.prepared_statements = [];
    this.ps_sqls = [];

    this.qc = QueryCrafter;
  }

  static async connect(config) {
    try {
      const client = new pg.Client({
        user: config.user,
        password: config.pass,
        host: config.host || "127.0.0.1",
        database: config.database,
        port: config.port || 5432
      });

      client.on('error', (err, client) => {
        console.error('Unexpected error on idle client', err)
        process.exit(-1)
      });

      await client.connect();

      const _this = new PGAura(client, config.database);
      return _this;
    } catch (e) {
      console.error(e.stack);
      process.exit(1);
    }
  }

  async table(name, columns) {
    try {
      return await Table.construct(name, columns, this);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async query(qstr) {
    return await this.client.query(qstr);
  }

  async disconnect() {
    try {
      await this.client.end();
    } catch (e) {
      console.error(e.stack);
    }
  }
}
