

export default class PraparedStatementManager {
  constructor(aura) {
    this.aura = aura;

    this.list = [];
    this.next_id = 0;
  }

  find(ps_id) {
    for (let ps of this.list) {
      if (ps.id === ps_id) return ps;
    }
    return undefined;
  }

  find_by_name(ps_name) {
    for (let ps of this.list) {
      if (ps.id === ps_name) return ps;
    }
    return undefined;
  }

  async craft(ps_params, ps_command, ps_conditions, ps_options) {
    const ps_id = `${ps_params}${ps_command}${ps_conditions}${ps_options}`;
    const existing_ps = this.find(ps_id);
    if (!existing_ps) {
      const nps_name = `PS_${this.next_id++}`;

      if (ps_conditions.length > 0) ps_conditions = ` WHERE${ps_conditions}`
      if (ps_options.length > 0) ps_options = ` ${ps_options}`
      const ps_str = `PREPARE ${nps_name}(${ps_params}) AS ${ps_command}${ps_conditions}${ps_options};`;

      const nps = { id: ps_id, name: nps_name, str: ps_str };

      this.list.push(nps);

      await this.aura.client.query(ps_str);
      return nps;
    } else {
      return existing_ps;
    }
  }
}
