
import PraparedStatement from './prepared/statement.mjs'

export default class QueryCrafter {

  static table() {

  }

  static prepared_statement(toid, statement, columns, where, options, values) {
    return new PraparedStatement(toid, statement, columns, where, options, values);
  }

  static craft(ps_params, ps_command, ps_conditions, ps_options) {
    const ps_name = `${ps_params}${ps_command}${ps_conditions}${ps_options}`
    const ps_str = `PREPARE ${ps_name}(${ps_params}) AS ${ps_command}${ps_conditions}${ps_options};`;
  }

}
