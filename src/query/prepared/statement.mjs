
import fs from 'fs'
import path from 'path'

import nunjucks from 'nunjucks'

import { URL } from 'url'
const __dirname = new URL('.', import.meta.url).pathname;

export default class PraparedStatement {
//            table, statement, cols, exprs, values
  constructor(table, statement, cols, exprs, values) {

    this.table = table;
    this.statement = statement;

    this.name = table.oid+"_"+statement;

    this.cols = cols;
    this.exprs = exprs;
    this.values = values;


    // TODO update specifics



    const columns = this.columns = table.get_columns(what_columns);

    let what_str = "_ALL";
    if (what != "*") {
      what_str = "";
      if (typeof what == "string" && what.toLowerCase().startsWith("count")) {
        what_str = "_COUNT";
        this.swhat = what;
      } else {
        for (var c = 0; c < columns.length; c++) {
          what_str += '_'+columns[c].name;
        }
      }
    }
    if (statement === 'update') {
      for (var c = 0; c < what.length; c++) {
        what_str += '_'+what[c];
      }
    }

    this.name += what_str;


    let where_str = "_ANY";
    if (where) {
      where = where.replace(/\s\s+/g, ' ');
      where = where.replace('( ', '(');
      where = where.replace(' )', ')');
      this.fly_conditions(where);
      where_str = "_"+where;
    }
    this.name += where_str;
  }

  fly_conditions(str, unnest) {
    var result = [];
    function set_param_column(param, column, to_array) {
      if (param.match(/ANY\(\$([0-9]+)\)/g)) {
        to_array = true;
        param = param.match(/(?<=ANY\()(.*)(?=\))/g)[0];
      }
      param = parseInt(param.replace('$', ''));
      var param_exists = false;
      for (var r = 0; r < result.length; r++) {
        if (result[r].param === param) {
          result[r].columns.push(column);
          result[r].to_array = to_array;
          param_exists = true;
          break;
        }
      }
      if (!param_exists) {
        result[r] = {
          param: param,
          columns: [column],
          to_array: to_array
        }
      }
    };

    var regex = /([^ ]+ *(?:ILIKE|LIKE|ALL|IN|<=|>=|!=|<>|@>|<@|&&|\|\||<|>|=) *(\$\d*|\w*\s?\(\$\d*\)))/g;
    var match;
    while (match = regex.exec(str)) {

      match = match[0].split(" ");


      let to_array = false;
      if (match[0].match(/(ARRAY\[)(.*)(\])/g)) to_array = true;
      var column = match[0].match(/(ARRAY\[)(.*)(\])/g) ? match[0].replace("ARRAY[", "").replace("]", "") : match[0];
      if (unnest && column == unnest.as) column = unnest.column;
      if (match[1].includes("<@")) {
        to_array = true;
      }
      var parameter = match[2];

      if (column.startsWith("(")) column = column.substring(1);
      set_param_column(parameter, column, to_array);
    }

    result.sort(function(a, b) {
      if (a.param < b.param) {
        return -1;
      } else if (a.param > b.param) {
        return 1;
      } else {
        return 0;
      }
    });

    let cols = [];
    for (var r = 0; r < result.length; r++) {
      for (var c = 0; c < this.table.columns.length; c++) {
        var col_name = this.table.columns[c].name;
        var col_type = this.table.columns[c].type;
        if (col_name == result[r].columns[0]) {
          if (col_type.toLowerCase() == "serial primary key") col_type = "integer"
          if (result[r].to_array) col_type += "[]";
          cols.push({ type: col_type, name: col_name });
        }
      }
    }
    this.arg_columns = cols;


    var types = [];
    var named_types = [];
    for (let c = 0; c < cols.length; c++) {
      let push_type = cols[c].type;
      if (unnest && cols[c].name == unnest.column) push_type = push_type.slice(0, -2);
      types.push(push_type);
      named_types.push(cols[c]);
    }
    this.arg_types = types;
    this.named_arg_types = named_types;
  }

  craft(index) {
    if (this.where || this.options) {
      this.condition_sql = this.sql_condition(this.where, this.options).wsql;
    }

    if (this.statement == "update") {
/*      let vlength = this.arg_types.length;
      for (var c = 0; c < this.columns.length; c++) {
        if (this.arg_types) {
          this.columns[c].index = vlength+c+1;
        } else {
          this.columns[c].index = c+1;
        }
        if (
          this.columns[c].type.endsWith("[]") &&
          typeof this.what[this.columns[c].name] == "string" &&
          this.what[this.columns[c].name].startsWith("||")
        ) {
          this.columns[c].action = "push";
          this.arg_types.push(this.columns[c].type.slice(0, -2));
        //  set_param_types.push(columns[c].type.slice(0, -2));
        } else {
          this.arg_types.push(this.columns[c].type);
       //   set_param_types.push(columns[c].type);
        }
      }*/
    }


    let unnest = (this.options && this.options.unnest) ? this.options.unnest : false;
    let from_elements = false;
    if (unnest) {
      delete this.options.unnest;
      from_elements = ", unnest("+unnest.column+") "+unnest.as;
      let ctype = table.get_column(unnest.column).type;
      ctype = ctype.slice(0, ctype.length-2);
  //    arg_types.push(ctype);
    }

    let distinct = this.options ? this.options.distinct : false;
    if (distinct) delete this.options.distinct;

    if (this.options && this.options.offset && this.arg_types) this.arg_types.push("bigint");
    
    let options_str = ' ';
    if (typeof this.options === 'object') {
      for (let o in this.options) {
        if (o.toLowerCase() == "order_by") {
          options_str += "ORDER BY "+this.options[o]+" ";
        }
      }

      for (let o in this.options) {
        if (o.toLowerCase() == "limit" ||
            o.toLowerCase() == "offset") {
          options_str += o+" "+this.options[o]+" ";
        }
      }
    }

    let cfg_columns = this.columns;
    let cfg_argtype = this.arg_types;
    if (this.statement === 'update') {
      cfg_columns = [];
      cfg_argtype = [];
      for (let set_what of this.what) {
        cfg_columns.push(set_what);
        const cname = set_what.match(/(^[a-z_]*)(?=[ =]*.*$)/g)[0];
        let ctype = undefined;
        for (let col of this.columns) {
          if (col.name === cname) {
            ctype = col.type;
            break;
          } 
        }
        if (
          !Array.isArray(this.values[this.what.indexOf(set_what)]) &&
          ctype.endsWith('[]')
        ) {
          ctype = ctype.slice(0, -2);
        }
        cfg_argtype.push(ctype);
      }
      cfg_argtype = [...cfg_argtype, ...this.arg_types]
    }

    let select_cfg = {
      table: this.table.name,
      columns: cfg_columns,
      prepared_statement: "PS_"+index,
      arg_types: cfg_argtype,
      condition: this.condition_sql,
      options: options_str || this.options,
      from_elements: from_elements,
      distinct: distinct,
      swhat: this.swhat
    };


    this.sql = nunjucks.renderString(
      fs.readFileSync(path.resolve(__dirname, `../templates/${this.statement}.psql.njk`), 'utf8'),
      select_cfg
    );

    return this.sql;

  }

  sql_condition(wstr, ostr) {
    let sql = wstr+"-"+ostr;
    for (var c = 0; c < this.table.conditions.length; c++) {
      if (this.table.conditions[c].sql == sql) {
        return this.table.conditions[c];
      }
    }
    var new_condition = { id: this.table.conditions.length, wsql: wstr };
    this.table.conditions.push(new_condition);
    return new_condition;
  }

  prepare_arg_string(values, columns) {
    if (values) {
      var string = '(';
      var first_value = true;
      for (var v = 0; v < values.length; v++) {
        var value = values[v];
        if (!first_value) {
          string += ', ';
        } else {
          first_value = false;
        }


        if (typeof value === 'string' || value instanceof String) {
          if (value.startsWith('||') && columns[v].type == "uuid") {
            string += "'"+value.substring(2)+"'::UUID";
          } else if (value.toLowerCase().endsWith("::uuid") || value.toLowerCase().endsWith("::uuid[]")) {
            string += value;
          } else {
            string += "'"+value+"'";
          }
        } else if (typeof value === 'array' || value instanceof Array || Array.isArray(value)) {
          string += "ARRAY [";
          for (var va = 0; va < value.length; va++) {
            if (va > 0) {
              string += ", '"+value[va]+"'";
            } else {
              string += "'"+value[va]+"'";
            }
          }
          string += "]";
          if (columns && columns[v]) {
            string += "::"+columns[v].type+"[]";
          }
        } else if (value && typeof value === 'object' && value.constructor === Object) {
          string += "'"+JSON.stringify(value)+"'::JSONB";
        } else {
          string += value;
        }
      }
      string += ')';
      return string;
    } else {
      return '';
    }
  }

  prepare_arg_values(values) {
    var arg_values = [];


    for (var c = 0; c < this.table.columns.length; c++) {
      var col_name = this.table.columns[c].name;
      var col_type = this.table.columns[c].type;
      for (var key in values) {
        if (col_name == key) {
          arg_values.push(values[key]);
          break;
        }
      }
    }
    return arg_values;
  }
}
