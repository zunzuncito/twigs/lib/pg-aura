
import PraparedStatement from './statement.mjs'

export default class DeleteStatement {
  static async prepare(table, where, values) {
    let prepared_sql;
    let qstr;
    try {
      let ps = await PraparedStatement.prepare( "delete", table, false, where);
      let psid = ps.id;
      prepared_sql = ps.sql;
      let prepared_statement = 'PS_'+psid;

      var argument_string = ps.crafter.prepare_arg_string(values);

      qstr = "EXECUTE "+prepared_statement+argument_string;
      return (await table.client.query(qstr)).rows;
    } catch (e) {
      console.log(prepared_sql);
      console.log(qstr);
      console.log("DELETE WHERE", where, "VALUES", values);
      console.error(e.stack);
      return false;
    }
  }
}
