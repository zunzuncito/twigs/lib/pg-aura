
import PraparedStatement from './statement.mjs'

export default class InsertStatement {
  static async prepare(table, cols, val_strs, vals) {

    let prepared_statement_sql;
    let query_string;
    try {
      if (!cols || cols.length === 0) {
        // TODO: CHECK WHETHER HAS ID
        return (await table.client.query(
          `INSERT INTO ${table.name} DEFAULT VALUES RETURNING ID;`
        )).rows[0].id;
      }

      const ps_command = `INSERT INTO ${table.name}(${cols}) VALUES(${val_strs})`;

      let ps = await PraparedStatement.prepare("insert", table, cols, val_strs, {}, vals);
      let psid = ps.id;
      prepared_statement_sql = ps.sql;
      let prepared_statement = 'PS_'+psid;

      var columns = table.get_columns(cols);

      var arg_str = ps.crafter.prepare_arg_string(vals, columns);
      query_string = "EXECUTE "+prepared_statement+arg_str;
      return (await table.client.query(query_string)).rows[0].id;
    } catch (e) {
      console.log("INSERT", obj);
      console.log("PREPARED STATEMENT");
      console.log(prepared_statement_sql);
      console.log("QUERY STRING");
      console.log(query_string);
      console.error(e.stack);
      return false;
    }
  }
}
