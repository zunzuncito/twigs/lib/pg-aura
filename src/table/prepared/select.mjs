
import PraparedStatement from './statement.mjs'

export default class SelectStatement {
  static async prepare(table, what, where, values, options) {
    if (!Array.isArray(values)) values = [values];


    const ps_command = `SELECT DISTINCT(${cols}) FROM table_name`;

    let query_string;
    let prepared_sql;
    try {
      if (where || options) {


        let ps = await PraparedStatement.prepare("select", table, what, where, options);
        var arg_types = false;
        if (where) {
          arg_types = ps.crafter.named_arg_types;
        }
        let psid = ps.id;
        prepared_sql = ps.sql;

        var prepared_statement = 'PS_'+psid;

        let value_columns = [];
        for (let c = 0; c < table.columns.length; c++) {
          for (let t = 0; t < arg_types.length; t++) {
            if (table.columns[c].name == arg_types[t].name) {
              value_columns.push(table.columns[c]);
            }
          }
        }
        var argument_string = ps.crafter.prepare_arg_string(values, value_columns);

        var qstr = "EXECUTE "+prepared_statement+argument_string;
        query_string = qstr
        const rows = (await table.client.query(qstr)).rows;

        return rows ?
          (
            rows.length > 0 ?
            rows : undefined
          ) : undefined;
      } else {
        let columns = table.get_columns(what);

        var qstr = "SELECT ";
        if (what === '*') {
          qstr += what;
        } else if (typeof what === 'string' && what.toLowerCase().startsWith('count(')) {
          qstr += what;
        } else {
          if (columns.length > 0) {
            qstr += columns[0].name;
            for (var c = 1; c < columns.length; c++) {
              qstr += ', '+columns[c].name;
            }
          }
        }
        qstr += ' FROM '+table.name;

        query_string = qstr;

        const rows = (await table.client.query(qstr)).rows;
        return rows ?
          (
            rows.length > 0 ?
            rows : undefined
          ) : undefined;
      }
    } catch (e) {
      console.error("table.select");
      if (prepared_sql) {
        console.log("prepared statement");
        console.log(prepared_sql);
      }

      console.log("query string");
      console.log(query_string);

      console.error(e.stack);
      return false;
    }
  }
}
