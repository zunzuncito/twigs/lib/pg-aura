

export default class PraparedStatement {
  static async prepare(statement, table, what, where, options, values) {
    let prep_sql = "";
    try {
      const aura = table.aura;
      await new Promise(function(resolve) {
        let interv = setInterval(function() {
          if (!aura.preparing_statement) {
            clearInterval(interv);
            resolve();
          }
        }, 100);
      });


      if (!statement || !table) {
        console.error(new Error("Cannot prepare a statement! No statement or table!"));
        return undefined;
      } else {
        aura.preparing_statement = true;


        const prep_stat = aura.qc.prepared_statement(
          table,
          statement,
          what,
          where,
          options,
          values
        );

        const columns = table.get_columns(what);
        
        let arg_types = prep_stat.arg_types;

        if (aura.prepared_statements.includes(prep_stat.name)) {
          let index = aura.prepared_statements.indexOf(prep_stat.name);

          aura.preparing_statement = false;
          return {
            id: index,
            sql: aura.ps_sqls[index],
            crafter: prep_stat
          }
        } else {
          let index = aura.prepared_statements.length;
 
          const prep_sql = prep_stat.craft(index);

          await aura.client.query(prep_sql);

          aura.prepared_statements.push(prep_stat.name);
          aura.ps_sqls[index] = prep_sql;

          aura.preparing_statement = false;
          return {
            id: index,
            sql: prep_sql,
            crafter: prep_stat
          }
        }
      }
    } catch (e) {
      console.log("ERR WTF ??");
      console.log(prep_sql);
      console.error(e.stack);
      return undefined;
    }
  }
}
