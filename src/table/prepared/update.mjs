
import PraparedStatement from './statement.mjs'

export default class UpdateStatement {
  static async prepare(table, set, where, values) {
    if (!Array.isArray(values)) values = [values];

    let query_string;
    let prepared_sql;
    try {

      let set_columns = [];
      for (let what of set) {
        what_columns.push(what.match(/(^[a-z_]*)(?=[ =]*.*$)/g)[0]);
      }

      let ps = await PraparedStatement.prepare("update", table, set, where, {}, values);
      let psid = ps.id;
      prepared_sql = ps.sql;

      let prepared_statement = 'PS_'+psid;

      var set_param_types = [];

      var columns = table.get_columns(set);
/*      for (var c = 0; c < columns.length; c++) {
        columns[c].index = c+1;
        set_param_types.push(columns[c].type);
      }*/

      var arg_types = false;
      var arg_columns = [];
      if (where) {
        arg_columns = ps.crafter.arg_columns;
        arg_types = ps.crafter.arg_types;
        arg_types.push.apply(arg_types, set_param_types);
      }

      for (let c = 0; c < arg_columns.length; c++) {
        let listed = false;
        for (let sc = 0; sc < columns.length; sc++) {
          if (arg_columns[c].name == columns.name) {
            listed = true;
          }
        }
        if (!listed) {
          columns.push(arg_columns[c]);
        }
      }

      var argument_string = ps.crafter.prepare_arg_string(values, columns);


      var qstr = "EXECUTE "+prepared_statement+argument_string;
      query_string = qstr;
      return (await table.client.query(qstr)).rows;
    } catch (e) {
      if (prepared_sql) {
        console.log("prep SQL", prepared_sql);
      }

      console.log("QSTR", query_string);
      console.error(e.stack);
      return false;
    }
  }
}
