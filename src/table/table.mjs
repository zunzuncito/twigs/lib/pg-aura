
import fs from 'fs'
import path from 'path'

import nunjucks from 'nunjucks'

import { URL } from 'url'
const __dirname = new URL('.', import.meta.url).pathname;

import ExprPrsr from '../expr/parser.mjs'

export default class Table {
  constructor(name, columns, aura) {
    this.aura = aura;

    this.name = name;

    this.columns = [];
    for (let key in columns) {
      const column = { name: key, type: columns[key]};

      const type_parts = column.type.split(' ');
      if (type_parts && type_parts.length > 1) {
        column.type = type_parts[0];
        type_parts.shift();
        column.default = type_parts.join(" ");
      }

      this.columns.push(column);
    }

    this.client = aura.client;

    this.conditions = [];
    this.prepared_statements = {
      insert: {},
      select: [],
      update: [],
      delete: []
    };
  }

  static async construct(name, columns, aura) {
    try {
      console.log(`PostgreSQL table ${name}`);

      if (!columns) {
        const query = `
          SELECT column_name, data_type, udt_name, character_maximum_length
          FROM information_schema.columns
          WHERE table_name = $1;
        `;
        const { rows } = await aura.client.query(query, [name]);
        columns = {};
        rows.forEach(row => {
          if (row.data_type === 'ARRAY') {
            columns[row.column_name] = row.udt_name.substring(1) + '[]';
          } else if (row.data_type === 'character varying' && row.character_maximum_length) {
            columns[row.column_name] = `varchar(${row.character_maximum_length})`;
          } else {
            columns[row.column_name] = row.data_type;
          }
        });
        console.debug(`Table Structure for ${name}:`);
        console.debug(columns);
      }

      const _this = new Table(name, columns, aura);
      
      const existing = await _this.client.query("SELECT column_name, data_type from information_schema.columns where table_name = '"+_this.name+"';");

      const table_ctx = {
        table: name
      };

      if (existing.rows.length > 0) {
        table_ctx.alter_columns = [];
        table_ctx.add_columns = [];
        table_ctx.drop_columns = [];

        for (let r = 0; r < existing.rows.length; r++) {
          const row = existing.rows[r];

          let drop = true;
          for (let c = 0; c < _this.columns.length; c++) {
            const column = _this.columns[c];
            column.name = column.name.toLowerCase();
            column.type = column.type.toLowerCase();

            if (column.name === row.column_name) {
              drop = false;
            }
          }

          if (drop) {
            const column = {
              name: row.column_name,
              type: row.data_type
            };
            table_ctx.drop_columns.push(column);
          }
        }

        for (let c = 0; c < _this.columns.length; c++) {
          const column = _this.columns[c];

          let add = true;
          for (let r = 0; r < existing.rows.length; r++) {
            const row = existing.rows[r];

            if (column.name === row.column_name) {
              add = false;
              if (column.type != row.data_type && table_ctx.force_alter) { // TODO: some kind of mechanism to control alteration
                table_ctx.alter_columns.push(column);
              }
            }
          }

          if (add) {
            table_ctx.add_columns.push(column);
          }
        }
      } else {
        table_ctx.columns = _this.columns
      }

      if (
        (table_ctx.columns && table_ctx.columns.length > 0) ||
        (table_ctx.alter_columns && (
          table_ctx.alter_columns.length > 0 ||
          table_ctx.add_columns.length > 0 ||
          table_ctx.drop_columns.length > 0
        ))
      ) {

        const template_path = path.resolve(__dirname, "templates/table.psql.njk");
        const template_src = fs.readFileSync(template_path, 'utf8');
        await _this.client.query(
          nunjucks.renderString(template_src, table_ctx)
        );
      }

      for (let c = 0; c < _this.columns.length; c++) {
        if (_this.columns[c].type === "serial") {
          _this.columns[c].type = "bigint";
        } else if (_this.columns[c].type.toLowerCase().includes("reference")) {
          _this.columns[c].type = _this.columns[c].type.split(' ')[0];
        }
      }

      _this.oid = (await _this.client.query("SELECT '"+_this.name+"'::regclass::oid;")).rows[0].oid;

      return _this;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async query(qstr) {
    return await this.aura.query(qstr);
  }

  async prepare_and_execute(ps_params, ps_command, ps_conditions, vals, options) {
    let ps_options = typeof options === 'string' ? ` ${options}` : ``;

    if (ps_params.length == 0) {
      if (!ps_options) ps_options = "";
      const exec_query_str = `${ps_command}${ps_options};`;
      try {
        return await this.aura.client.query(exec_query_str);
      } catch (e) {
        throw new Error(`pg-aura: query failed:
  <<<<<<<<<<<

    ${exec_query_str}

  >>>>>>>>>>>
  ${e.message}
        `);
        return;
      }
    }

    let ps = undefined;
    try {
      ps = await this.aura.psmg.craft(ps_params, ps_command, ps_conditions, ps_options);
    } catch (e) {
      console.log(ps_params, ps_command, ps_conditions, ps_options);
      throw e;
      return;
    }

    const ps_vals = [...vals];
    ExprPrsr.parse_param_val(ps_vals, ps_params);
    const exec_query_str = `EXECUTE ${ps.name}(${ps_vals});`;

    try {
      return await this.aura.client.query(exec_query_str);
    } catch (e) {
      throw new Error(`pg-aura: prepared statement:
<<<<<<<<<<<

  ${ps.str}

>>>>>>>>>>>
execution failed:
<<<<<<<<<<<

  ${exec_query_str}

>>>>>>>>>>>
${e.message}
      `);
      return;
    }
 
  }

  async insert(cols, val_exprs, vals, options) {
    try {
      let query_result = undefined;
      if (!cols && !val_exprs && !vals) {
        query_result = (await this.client.query(
          `INSERT INTO ${this.name} DEFAULT VALUES ${options};`
        ));
      } else {
        const ps_params = Array(vals.length);
        ExprPrsr.parse_val(cols, val_exprs, vals, ps_params, this);
        const ps_command = `INSERT INTO ${this.name}(${cols}) VALUES(${val_exprs})`;
        query_result = await this.prepare_and_execute(ps_params, ps_command, ``, vals, options);
      }
      return ExprPrsr.parse_return(query_result, options);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async select(cols, where_expr, vals, options) {
    try {
      if (!vals) vals = [];
      const ps_params = Array(vals.length);
      ExprPrsr.parse_where(where_expr, vals, ps_params, this);
      const ps_command = `SELECT ${cols} FROM ${this.name}`;
      const ps_conditions = typeof where_expr === 'string' ? ` ${where_expr}` : ``;
      return (await this.prepare_and_execute(ps_params, ps_command, ps_conditions, vals, options)).rows;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async update(set_exprs, where_expr, vals, options) {
    try {
      const ps_params = Array(vals.length);
      ExprPrsr.parse_set(set_exprs, vals, ps_params, this);
      ExprPrsr.parse_where(where_expr, vals, ps_params, this);
      const ps_command = `UPDATE ${this.name} SET ${set_exprs}`;
      const ps_conditions = typeof where_expr === 'string' ? ` ${where_expr}` : ``;
      return await this.prepare_and_execute(ps_params, ps_command, ps_conditions, vals, options);
    } catch (e) {
      console.error(e.stack);
    }
  }

  async delete(where_expr, vals, options) {
    try {
      if (where_expr.toLowerCase() == "all") {
        return (await this.client.query(
          `DELETE FROM ${this.name};`
        ));
      } else {
        const ps_params = Array(vals.length);
        ExprPrsr.parse_where(where_expr, vals, ps_params, this);
        const ps_command = `DELETE FROM ${this.name}`;
        const ps_conditions = typeof where_expr === 'string' ? ` ${where_expr}` : ``;
        return await this.prepare_and_execute(ps_params, ps_command, ps_conditions, vals, options);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  column(name) {
    for (let col of this.columns) {
      if (col.name === name) return col;
    }
    throw new Error(`Table '${this.name}' does not have a column named '${name}'!`);
  }

  get_columns(expressions) {
    if (Array.isArray(expressions)) {
      var columns = [];
      for (var c = 0; c < this.columns.length; c++) {
        var col_name = this.columns[c].name;
        var defined = false;
        for (var o = 0; o < expressions.length; o++) {
          const regex = new RegExp(`^${col_name}[ ]*=.*$`);
          if (col_name === expressions[o] || regex.test(expressions[o])) {
            defined = true;
          }
        }
        if (defined) {
          columns.push(this.columns[c]);
        }
      }
      return columns;
    } else {
      return this.columns;
    }
  }

}
