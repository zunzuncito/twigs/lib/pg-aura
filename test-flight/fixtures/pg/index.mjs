
import pg from 'pg'

import fs from 'fs'
import path from 'path'

export default class PgFixture {
  constructor(cfg, tools, client) {
    this.cfg = cfg;
    this.tools = tools;
    this.client = client;
  }

  static async construct(cfg, tools) {
    try {
      const pg_cfg = JSON.parse(fs.readFileSync(path.resolve(
        cfg.twig.cwd, cfg.twig.custom_env || '.pg-env.json'
      )));

      const client = new pg.Client({
        user: pg_cfg.db_user,
        password: pg_cfg.db_pwd,
        host: "127.0.0.1",
        database: pg_cfg.db_name,
        port: 5432
      });

      client.on('error', (err, client) => {
        console.error('Unexpected error on idle client', err)
        process.exit(-1)
      });

      await client.connect();

      const _this = new PgFixture(cfg, tools, client);
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async teardown() {
    try {
      await this.client.end();
    } catch (e) {
      console.error(e.stack);
    }
  }
}
