
import fs from 'fs'
import path from 'path'

import Table from "./table.mjs"


export default class AuraMock {
  constructor(cfg, tools) {
    this.cfg = cfg;
    this.tools = tools;

    this.events = [];

    const _this = this;

    this.client = {
      query: async (qstr) => {
        _this.events.push({
          event: "query",
          data: qstr
        });
      },
      disconnect: async () => {
        _this.events.push({
          event: "disconnect"
        });
      }
    }

  }

  static async construct(cfg, tools) {
    try {
      const _this = new AuraMock(cfg, tools);
      _this.events.push({
        event: "connect"
      });
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async table(name, columns) {
    try {
      return await Table.construct(name, columns, this);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async query(qstr) {
    return await this.client.query(qstr);
  }

  async disconnect() {
    try {
      await this.client.end();
    } catch (e) {
      console.error(e.stack);
}
  }

}
