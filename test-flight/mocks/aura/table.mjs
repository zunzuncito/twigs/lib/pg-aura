
import fs from 'fs'
import path from 'path'

export default class TableMock {
  constructor(name, columns, aura) {
    this.aura = aura;
    this.name = name;

    this.columns = [];
    for (let key in columns) {
      const column = { name: key, type: columns[key]};

      const type_parts = column.type.split(' ');
      if (type_parts && type_parts.length > 1) {
        column.type = type_parts[0];
        type_parts.shift();
        column.default = type_parts.join(" ");
      }

      this.columns.push(column);
    }

    this.client = aura.client;
  }

  static async construct(name, columns, aura) {
    try {
      const _this = new TableMock(name, columns, aura);
      _this.aura.events.push({
        event: "create_table",
        data: [name, columns]
      });
      return _this;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }


  async insert(cols, val_exprs, vals, options) {
    try {
      this.aura.events.push({
        event: "insert",
        data: [cols, val_exprs, vals, options]
      });
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async select(cols, where_expr, vals, options) {
    try {
      this.aura.events.push({
        event: "select",
        data: [cols, where_expr, vals, options]
      });
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async update(set_exprs, where_expr, vals, options) {
    try {
      this.aura.events.push({
        event: "update",
        data: [set_exprs, where_expr, vals, options]
      });
    } catch (e) {
      console.error(e.stack);
    }
  }

  async delete(where_expr, vals, options) {
    try {
      this.aura.events.push({
        event: "delete",
        data: [where_expr, vals, options]
      });
    } catch (e) {
      console.error(e.stack);
    }
  }

  column(name) {
    for (let col of this.columns) {
      if (col.name === name) return col;
    }
    return undefined;
  }

  get_columns(expressions) {
    if (Array.isArray(expressions)) {
      var columns = [];
      for (var c = 0; c < this.columns.length; c++) {
        var col_name = this.columns[c].name;
        var defined = false;
        for (var o = 0; o < expressions.length; o++) {
          const regex = new RegExp(`^${col_name}[ ]*=.*$`);
          if (col_name === expressions[o] || regex.test(expressions[o])) {
            defined = true;
          }
        }
        if (defined) {
          columns.push(this.columns[c]);
        }
      }
      return columns;
    } else {
      return this.columns;
    }
  }

}
